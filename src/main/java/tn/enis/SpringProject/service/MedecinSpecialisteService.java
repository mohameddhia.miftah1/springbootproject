package tn.enis.SpringProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.enis.SpringProject.dao.MedecinSpecialisteRepository;
import tn.enis.SpringProject.entities.MedecinSpecialiste;

@Service
public class MedecinSpecialisteService implements IMedecinSpecialisteService {

	@Autowired
	MedecinSpecialisteRepository medecinSpecialisteRepository;

	@Override
	public MedecinSpecialiste getOneMedecinSpecialiste(Long id) {
		return medecinSpecialisteRepository.findById(id).get();
	}

	@Override
	public List<MedecinSpecialiste> getAllMedecinSpecialiste() {
		return medecinSpecialisteRepository.findAll();
	}

	@Override
	public void addMedecinSpecialiste(MedecinSpecialiste medecinSpecialiste) {
		medecinSpecialisteRepository.saveAndFlush(medecinSpecialiste);
	}

	@Override
	public void updateMedecinSpecialiste(Long id, MedecinSpecialiste medecinSpecialiste) {
		medecinSpecialiste.setId(id);
		medecinSpecialisteRepository.saveAndFlush(medecinSpecialiste);
	}

	@Override
	public String deleteMedecinSpecialiste(Long id) {
		medecinSpecialisteRepository.deleteById(id);
		medecinSpecialisteRepository.flush();
		return "medecin specialiste deleted";
	}
}
