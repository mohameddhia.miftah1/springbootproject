package tn.enis.SpringProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.enis.SpringProject.dao.UserRepository;
import tn.enis.SpringProject.entities.User;

@Service
public class UserService implements IUserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public User getOneUser(String username) {
		return userRepository.findById(username).get();
	}

	@Override
	public List<User> getAllUser() {
		return userRepository.findAll();
	}

	@Override
	public void addUser(User user) {
		userRepository.saveAndFlush(user);
	}

	@Override
	public void updateUser(String username, User user) {
		user.setUsername(username);
		userRepository.saveAndFlush(user);
	}

	@Override
	public String deleteUser(String username) {
		userRepository.deleteById(username);
		userRepository.flush();
		return "user supprimé avec succé";
	}
}
