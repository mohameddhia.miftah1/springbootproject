package tn.enis.SpringProject.service;

import java.util.List;

import tn.enis.SpringProject.entities.User;

public interface IUserService {

	public User getOneUser(String username);

	public List<User> getAllUser();

	public void addUser(User user);

	public void updateUser(String username, User user);

	public String deleteUser(String username);

}
