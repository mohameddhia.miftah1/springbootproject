package tn.enis.SpringProject.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String username;
	private String password;
	private boolean actived;
	private String role;

	@OneToOne
	private Patient patientUser;

	@OneToOne
	private Medecin medecinUser;

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public User(String username, String password, boolean actived, String role) {
		super();
		this.username = username;
		this.password = password;
		this.actived = actived;
	}

	public User() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActived() {
		return actived;
	}

	public void setActived(boolean actived) {
		this.actived = actived;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Patient getPatient() {
		return patientUser;
	}

	public void setPatient(Patient patient) {
		this.patientUser = patient;
	}

	public Medecin getMedecin() {
		return medecinUser;
	}

	public void setMedecin(Medecin medecin) {
		this.medecinUser = medecin;
	}
}
