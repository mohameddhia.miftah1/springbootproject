package tn.enis.SpringProject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.enis.SpringProject.entities.MedecinSpecialiste;

public interface MedecinSpecialisteRepository extends JpaRepository<MedecinSpecialiste, Long> {

}
