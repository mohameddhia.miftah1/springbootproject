package tn.enis.SpringProject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tn.enis.SpringProject.entities.Medecin;

public interface MedecinRepository extends JpaRepository<Medecin, Long> {

	@Query("select m from Medecin m where upper(m.nom) like concat(upper(:nom),'%')")
	public List<Medecin> findMedecinByLastName(@Param("nom") String nom);

	@Query("SELECT m FROM Medecin m WHERE lower(m.prenom) like concat(LOWER(:prenom),'%')")
	public List<Medecin> findMedecinByName(@Param("prenom") String prenom);

	@Query("select m from Medecin m order by nom ASC")
	public List<Medecin> sortByLastName();

	@Query("select m from Medecin m order by prenom ASC")
	public List<Medecin> sortByName();

}
